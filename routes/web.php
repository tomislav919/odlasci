<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index');
});

Route::get('/report', 'AdminController@redirectToReport');


Route::post('/formcontroller', 'FormController@formSubmit');

Route::post('/submitForgotten', 'ForgottenFormController@formsubmit');

Route::get('/autoClose', 'AutoCloseController@autoClose');

