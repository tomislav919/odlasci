

function checkUserEnter() {
    $('#spinner').show();
    //jquery serijalizacija forme, pokupimo podatke od input polja (polja moraju imati "name")
    let dataFormControler = $('#formControler').serializeArray();
    /* u skriveno input polje spremam posljednji korišteni user id */
    let user_id = $("#user_id").val();
    $('#lastUsedUserId').val(user_id);

    $.ajax({

        url: 'formcontroller',
        type: 'POST',
        data: dataFormControler,
        success: function (res) {

            $('#spinner').hide();
            message(res);
            clearTimeout(msgAlertDivTimeout);
            msgAlertDivTimeout = setTimeout(function () {
                $('#alertDiv').attr('hidden', true);
            }, 5000);

            document.getElementById("arrivalDeparture").value = 0;
            document.getElementById("user_id").value = "";

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            console.log(errorThrown);
        }

    });
}

