
let msgAlertDivTimeout1;
let clockTimeout;

function message(data) {

    $('#alertDiv').attr('hidden', true);
    $('#alertDiv').show();


    if(data.data.forgottenArrival){
        /* privremeno disable focus input field */

        $('body').off('click');
        $('#alertDivForm').removeAttr('hidden');
        setTimeout(function () {
            $( "#timeLogin" ).focus();
            $( "html" ).trigger('click');
        },10);

        /* inicijaliziram date-time picker u formi zaboravljenog dolaska (<input type="text" id="timeLogin" name="timeLogin">) */
        let date = new Date();
        let hour = date.getHours();
        let minutes = date.getMinutes();
        $("#timeLogin").datetimepicker({
            format: "HH:mm",
            maxDate: moment(
                {
                    h: hour,
                    m: minutes
                }
            )
        });


    }else{
        $('#alertDivForm').attr('hidden', true);
    }

    $('#alertDivContent').html(data.data.msg);

    $("#alertDiv")
        .removeClass()
        .addClass(data.data.class)
        .removeAttr("hidden");
    $('#alertDiv2').attr('hidden', true);

    if(data.data.hideClock){
        $('#flipClock').hide();
        clearTimeout(clockTimeout);
        clockTimeout = setTimeout(function () {
            $('#flipClock').show();
        }, 7000);
    } else {
        clearTimeout(clockTimeout);
        $('#flipClock').show();
    }


    $('#sound')
        .attr('src', data.data.sound)
        .removeAttr('autoplay')
        .attr('autoplay', '1');

    $('#alertDiv2').hide();

    if(data.data.forgottenArrival){
        clearTimeout(msgAlertDivTimeout1);
    } else {
        clearTimeout(msgAlertDivTimeout1);
        msgAlertDivTimeout1 = setTimeout(function () {
            $('#alertDiv').fadeOut('fast');
        }, 7000);
    }

}
