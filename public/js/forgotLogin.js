

function forgotLogin() {
    $('#spinner').show();
    //jquery serijalizacija forme, pokupimo podatke od input polja (polja moraju imati "name")
    let dataForgotLogin = $('#formForgotLogin').serializeArray();
   /* let user_id = $('#lastUsedUserId').val();

    user_idObj = {
        name: "user_id",
        value: user_id
    };
    dataForgotLogin.push(user_idObj);
    console.log(dataForgotLogin);
    */

      $.ajax({

        url: 'submitForgotten',
        type: 'POST',
        data: dataForgotLogin,
        success: function (res) {
            $('#spinner').hide();
           message(res);
           /* vraćam fokus na defaultno input polje i re aktivacija event listenera za fokus */
            $( "#user_id" ).focus();
            $("body").click(function(){
                $( "#user_id" ).focus();

            });
            $('#flipClock').show();
        },

        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            console.log(errorThrown);
        }

    });
}
