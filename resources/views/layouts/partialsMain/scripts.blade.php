<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{!! asset('datetimepicker/bootstrap-datetimepicker.min.js') !!}"></script>
<script src="{!! asset('clock/flipclock.js') !!}"></script>
<script src="{!! asset('js/flipClock.js') !!}"></script>
<script src="https://kit.fontawesome.com/8d5510c824.js" crossorigin="anonymous"></script>


<!-- InputMask -->


<script>

    //reset the app
    $(document).keyup(function(e) {
        if (e.key === "Escape") { // escape key maps to keycode `27`
            location.reload();
        }
    });

    /* onemogućavam defaultno submitanje forme */
    $(document).on("keydown", ":input:not(textarea)", function(event) {
        if (event.key == "Enter") {
            event.preventDefault();
        }
    });


    document.getElementById('formControler').addEventListener('keyup', function(event){
        if(event.key==="Enter"){
            checkUserEnter();
        }
    });

    document.getElementById('formForgotLogin').addEventListener('keyup', function(event){
        event.preventDefault();
        if(event.key==="Enter"){
            /* kupim user_id i postavljam ga u skriveno polje "lastUsedId"  */


        }
    });



    const user_id = document.getElementById('user_id');

    let msgAlertDiv2Timeout;
    let msgAlertDivTimeout;
    let inputValueTimeout;


    user_id.addEventListener('keyup', function(event){
        console.log(event.keyCode);

        event.preventDefault();

        var alertDiv2 = document.getElementById("alertDiv2");

        //event hit 1 or 1 on numpad
        if (event.keyCode == 32) {
            let alertDiv = document.getElementById('alertDiv');
            alertDiv.setAttribute('hidden', 'hidden');
            alertDiv2.removeAttribute('hidden');
            $('#alertDiv').hide();
            $('#flipClock').show();

            document.getElementById("arrivalDeparture").value = 1;
            document.getElementById("user_id").value = "";
            alertDiv2.style.display = "block";


            clearTimeout(msgAlertDiv2Timeout);
            msgAlertDiv2Timeout = setTimeout(function () {
                    $('#alertDiv2').fadeOut('fast');
                }, 7000);

            clearTimeout(inputValueTimeout);
            inputValueTimeout = setTimeout(function ()
            {
                document.getElementById("arrivalDeparture").value = 0;
            }, 7000);
        }
    });
</script>

<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "40rem";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>







<script src="{!! asset('js/focusField.js') !!}"></script>

<script src="{!! asset('js/checkUserEnter.js') !!}"></script>
<script src="{!! asset('js/forgotLogin.js') !!}"></script>
<script src="{!! asset('js/message.js') !!}"></script>

