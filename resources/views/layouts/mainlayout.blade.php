<!DOCTYPE html>
<html class="maxHeight" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
@include('layouts.partialsMain.head')
</head>

<body class="maxHeight backgroundColor">

@yield('content')


@include('layouts.partialsMain.scripts')

</body>
</html>
