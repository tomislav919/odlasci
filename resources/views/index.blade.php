@extends('layouts.mainlayout')

@section('content')



<div>
    <div class="col-md-12">
        <img class="logo" src="{{url('/images/logo.png')}}" />
    </div>

    <div class="clock-container" id="flipClock">
        <div class="clock" style="margin:0 auto;"></div>
    </div>
  <div class="col-md-12" >
      <form id="formControler" method="post">
          <input class="opacity" autocomplete="off" type="text" name="user_id" id="user_id" value="" autofocus size="1"><br>
            <input class="opacity" autocomplete="off" type="text" name="arrivalDeparture" id="arrivalDeparture" value="0" autofocus size="1"><br>
      </form>
  </div>

    <div class="col-md-12">
        <div class="col-md-4">
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <ul class="sidenav-list">
                    <li class="sidenav-list-item">
                        <p>
                            - za prijavu/odjavu sa posala pritisnite tipku <strong>"SPACE"</strong> te prislonite ključić na čitač
                        </p>
                    </li>
                    <li class="sidenav-list-item">
                        <p>
                            - za odlazak/dolazak s pauze samo prislonite <strong>"ključić"</strong> na čitač
                        </p>
                    </li>
                    <li class="sidenav-list-item">
                        <p>
                            - za zatvaranje prozora pritisnite tipku <strong>"esc"</strong>
                        </p>
                    </li>
                    <li class="sidenav-list-item">
                        <p>
                            - u slučaju naknadnog upisa početka radnog vremena, sa mišom odaberite sat i minute dolaska te kliknite na potvrdi
                        </p>
                    </li>
                </ul>
            </div>
            <span class="sidenav-instructions" onclick="openNav()"><i class="fas fa-angle-double-right"></i></span>
        </div>
    </div>

    <div class="col-md-12">
      <div class="alert alert-secondary center" id="alertDiv2" role="alert" style="display: none;">
          <center><span class="msg">Prijava/Odjava</span><br><br> Prislonite ključić za prijavu ili odjavu s posla</center>
      </div>

      <div class="" id="alertDiv" role="alert">
          <div id="alertDivContent"></div>
          <div id="alertDivForm" hidden>
              <form id="formForgotLogin">

                  <small>*odaberite vrijeme dolaska na posao</small>
                  <input type="text" id="timeLogin" name="arrivalTime" class="form-control">

                  <a class="btn btn-success btn-forgot" onclick="forgotLogin()">Potvrdi</a>
                  <input type="hidden" id="lastUsedUserId" name="user_id">
              </form>
          </div>
      </div>
    </div>
      <div><audio id="sound" src="/sounds/dolazak.wav" /></div>
  </div>
</div>


<div id="spinner" style="display:none;">
    <img src="{{url('/images/spinner.svg')}}" alt="spinner" class="spinner" />
</div>


@endsection
