<div class="HelpLegend">
    <div class="d-flex align-items-stretch">
        <div class="align-self-center p-2 mr-3">
            <img src="{{url('/images/1-icon.png')}}" class="keyboardIcon" style="width: 80px"/>
        </div>
        <div class="flex-grow-1 align-self-center">
            <p>
                Pritisnite tipku "1" za prijavu na posao i odlazak sa posla te prislonite ključić na čitač
            </p>
        </div>
    </div>
    <br>

    <div class="d-flex align-items-stretch">
        <div class="align-self-center p-2 mr-3">
            <img src="{{url('/images/esc-icon.png')}}" class="keyboardIcon" style="width: 80px">
        </div>
        <div class="flex-grow-1 align-self-center">
            <p>
                Za izlaz i gašenje prozora pritisnite tipku "esc"</p>
            <p>Za odlazak/dolazak s pauze prislonite ključić na čitač</p>
        </div>
    </div>

</div>
