<?php

namespace App\Classes;

use App\CheckinCheckout;
use App\User;

class GetData
{

    public static function firstCheckinToday()
    {


    }

    public static function lastCheckoutToday()
    {


    }

    public static function getLastArrival($userId){
        return CheckinCheckout::latest()->where('user_keyId', '=', $userId)->whereNotNull('arrival')->first();
    }

    public static function getLastDeparture($userId){
        return CheckinCheckout::latest()->where('user_keyId', '=', $userId)->whereNotNull('departure')->first();
    }

    public static function getLastCheckin($userId){
        return CheckinCheckout::latest()->where('user_keyId', '=', $userId)->whereNotNull('checkin')->first();
    }

    public static function getLastCheckout($userId){
        return CheckinCheckout::latest()->where('user_keyId', '=', $userId)->whereNotNull('checkout')->first();
    }

    public static function getUserFromUsers($userId){
        return User::where('keyId', '=', $userId)->get();

    }
}
