<?php

namespace App\Classes;

use App\CheckinCheckout;
use App\User;
use App\Classes\GetData;


class Message
{

    public static function message ($userId, $onBreakSum, $breakTimeAllowed, $type){

        $user = User::where('keyId', '=', $userId)->first();

        if($onBreakSum == true){
            $breakLeft = $breakTimeAllowed - $onBreakSum;
            if($breakLeft > 0){
                $breakLeftMinutes = gmdate("i", $breakLeft);
                $timeLeftMsg = 'Preostalo vrijeme za pauzu: ' . $breakLeftMinutes . ' min';
            } else if($breakLeft <= 0){
                $timeLeftMsg = 'Potrošili ste cijelu pauzu!';
            }


        }

        switch ($type){
            case 0:
                $message = (object) [];
                $message->class = 'alert alert-secondary center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/kljucNeRadi.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->msg = '<center><span class="msgSmall">Vaš ključić ne radi ili nema unešenog korisnika, kontaktirajte administratora!</span></center>';
                $message->forgottenArrival = false;
                $message->hideClock = false;

                return $message;
                break;

            case 1:
                $message = (object) [];
                $message->class = 'alert alert-secondary center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/dolazak.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                if($user->gender == 0){
                    $message->msg = '<center><span class="msg">Dobro došao</span><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                } else {
                    $message->msg = '<center><span class="msg">Dobro došla</span><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                }

                $message->forgottenArrival = false;
                $message->hideClock = false;

                return $message;
                break;

            case 2:
                $message = (object) [];
                $message->class = 'alert alert-secondary center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/odlazak.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->msg = '<center><span class="msg">Doviđenja</span><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span><br><br><span class="msgSmall">Ugodan ostatak dana!</span></center>';
                $message->forgottenArrival = false;
                $message->hideClock = false;

                return $message;
                break;

            case 3:
                $message = (object) [];
                $message->class = 'alert alert-success center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/checkout.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->forgottenArrival = false;
                $message->hideClock = true;


                if(isset($timeLeftMsg)){
                    $message->msg = '<center><span class="msg">Početak pauze</span><br><br><br><span class="msgTime">' . $timeLeftMsg . '</span><br><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                } else {
                    $message->msg = '<center><span class="msg">Početak pauze</span><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                }

                return $message;
                break;

            case 4:
                $message = (object) [];
                $message->class = 'alert alert-danger center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/checkin.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->forgottenArrival = false;
                $message->hideClock = true;

                if(isset($timeLeftMsg)){
                    $message->msg = '<center><span class="msg">Kraj pauze</span><br><br><br><span class="msgTime">' . $timeLeftMsg . '</span><br><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                } else {
                    $message->msg = '<center><span class="msg">Kraj pauze</span><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                }

                return $message;
                break;

            case 5:
                $message = (object) [];
                $message->class = 'alert alert-danger center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/odlazak.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->msg = '<center><span class="msg">Odlazak s posla, niste zatvorili zadnju pauzu, zatvorena je automatski</span><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                $message->forgottenArrival = false;
                $message->hideClock = false;

                return $message;
                break;

            case 6:
                $message = (object) [];
                $message->class = 'alert alert-danger center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/kljucNeRadi.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->msg = 'Nepredvidljiva greška, drugo polje nije 0 ili 1. Pokušajte opet!';
                $message->forgottenArrival = false;
                $message->hideClock = false;

                return $message;
                break;

            case 7:
                $message = (object) [];
                $message->class = 'alert alert-danger center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/kljucNeRadi.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->msg = '<center><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span><br><br><span class="msgSmall">Niste se prijavili prilikom dolaska na posao! Prijavite svoj dolazak kako bi mogli koristit pauzu.</span><br><br></center>';
                $message->forgottenArrival = true;
                $message->hideClock = false;

                return $message;
                break;

            case 8:
                $message = (object) [];
                $message->class = 'alert alert-danger center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/kljucNeRadi.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->msg = '<center><span class="msg">Prijavite se sa 1 za dolazak/odlazak na posao</span><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                $message->forgottenArrival = true;
                $message->hideClock = false;

                return $message;
                break;

           case 9:
               $message = (object) [];
               $message->class = 'alert alert-success center';
               $message->id = 'alertDiv';
               $message->sound = '/sounds/checkout.wav';
               $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
               $message->msg = '<center><span class="msg">Početak pauze</span><br><br><br><span class="msgTime">Imate 30 minuta pauze</span><br><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
               $message->forgottenArrival = false;
               $message->hideClock = true;

               if(isset($timeLeftMsg)){
                   $message->msg = '<center><span class="msg">Početak pauze</span><br><br><br><span class="msgTime">' . $timeLeftMsg . '</span><br><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
               } else {
                   $message->msg = '<center><span class="msg">Početak pauze</span><br><br><br><span class="msgTime">Imate 30 minuta pauze</span><br><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
               }

               return $message;
               break;

            case 10:
                $message = (object) [];
                $message->class = 'alert alert-danger center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/kljucNeRadi.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->msg = '<center><span class="msg">Vrijeme prijave ne može biti manje od zadnje odjave. Izađite sa "ESC" i unesite ispravno vrijeme!</span><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                $message->forgottenArrival = false;
                $message->hideClock = false;

                return $message;
                break;

            case 11:
                $message = (object) [];
                $message->class = 'alert alert-danger center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/kljucNeRadi.wav';
                $message->denyMsg = '<script>document.getElementById("alertDiv2").style.display = "none";document.getElementById("alertDiv3").style.display = "none";</script>';
                $message->msg = '<center><span class="msg">Došlo je do pogreške, osvježite aplikaciju sa "esc" pa pokušajte ponovno!</span><br><br><span class="msgName">' . $user->name . ' ' . $user->lastName . '</span></center>';
                $message->forgottenArrival = false;
                $message->hideClock = false;

                return $message;
                break;

            default:
                $message = (object) [];
                $message->class = 'alert alert-secondary center';
                $message->id = 'alertDiv';
                $message->sound = '/sounds/kljucNeRadi.wav';
                $message->msg = 'Greška kod kreiranja poruke!';
                $message->denyMsg = 0;
                $message->forgottenArrival = false;
                $message->hideClock = false;

                return $message;

        }
    }

}
