<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CheckinCheckout extends Model
{
    protected $fillable = [
        'user_keyId',
        'checkin',
        'checkout',
        'arrival',
        'departure',
        'onBreakTimeStamp',
        'autoClosed',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'keyId', 'user_keyId');
    }

    public static function submitArrival($userId, $now)
    {
        $user = User::where('keyId', $userId)->first();

        $chekinChekout = new CheckinCheckout(['user_keyId'=>$user, 'arrival'=> $now]);

        $user->CheckinCheckout()->save($chekinChekout);
    }

    public static function submitDeparture($userId, $now)
    {

        $user = User::where('keyId', $userId)->first();
        $checkinCheckout = CheckinCheckout::latest()->where('user_keyId', '=', $userId)->whereNotNull('arrival')->first();
        $checkinCheckout->departure = $now;


        $user->CheckinCheckout()->save($checkinCheckout);
  }

    public static function submitCheckinAndOnBreak($userId, $now, $onBreakTimestamp, $autoClosed)
    {
        //Insert checkin and break
        $user = User::where('keyId', $userId)->first();

        $checkinCheckout = CheckinCheckout::latest()->where('user_keyId', '=', $userId)->whereNotNull('checkout')->first();
        $checkinCheckout->checkin = $now;
        $checkinCheckout->onBreakTimestamp = $onBreakTimestamp;

        if($autoClosed == 1){
            $checkinCheckout->autoClosed = 1;
        }

        $user->CheckinCheckout()->save($checkinCheckout);

    }

    public static function submitCheckout($userId, $now)
    {
        $user = User::where('keyId', $userId)->first();

        $chekinChekout = new CheckinCheckout(['user_keyId'=>$user, 'checkout'=> $now]);

        $user->CheckinCheckout()->save($chekinChekout);
    }
}
