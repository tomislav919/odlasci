<?php

namespace App\Http\Controllers;

use App\CheckinCheckout;
use App\Classes\Message;
use App\User;
use App\Classes\GetData;
use App\Classes\CalcHelper;
use App\Mail\WarningMail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\Message as MessageResource;

class FormController extends Controller
{
    public function formSubmit(Request $req) {


            $userId = $req['user_id'];
            $arrivalDeparture = $req['arrivalDeparture'];
            $now = Carbon::now();

            $lastCheckin = GetData::getLastCheckin($userId)['checkin'];
            $lastCheckout = GetData::getLastCheckout($userId)['checkout'];

            $lastArrival = GetData::getLastArrival($userId)['arrival'];
            $lastDeparture = GetData::getLastDeparture($userId)['departure'];

            $lastRowCheckin = CheckinCheckout::latest()->where('user_keyId', '=', $userId)->first()['checkin'];
            $lastRowCheckout = CheckinCheckout::latest()->where('user_keyId', '=', $userId)->first()['checkout'];

            $user = GetData::getUserFromUsers($userId);
            $userMail = User::where('keyId', $userId)->first();


            //trajanje pauze, tj. koliko djelatnici smiju biti na pauzi
            $onBreakSum = 0;
            $breakTimeAllowed = 1800;
            $mailTo = 'adriano@inmobile-accessories.hr';

            //provjera da li je ključić povezan sa korisnikom u bazi, ako nije vraća error
            if ($user->isEmpty()) {
                //return to the front page with error "Ovaj ključić nije ispravan ili korisnik za njega nije napravljen"
                $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 0);
                return new MessageResource($msg);
            }

            switch ($arrivalDeparture) {
                //prijava sa praznom inpit fieldom, tj sa 0 (checkin ili checkout)
                case 0:
                    if (($lastDeparture < $lastArrival) && ($lastArrival < $now)) {
                        //insert as first checkout today (početak pauze)
                        if ($lastCheckout < $lastArrival) {
                            CheckinCheckout::submitCheckout($userId, $now);
                            $onBreakSum = CalcHelper::timeOnBreakSumForToday($userId);
                            $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 9);
                            return new MessageResource($msg);

                            //insert into checkin (kraj pauze)
                        } else if ($lastCheckin < $lastCheckout) {
                            $onBreakTimestamp = CalcHelper::dateStringDiffInTimestamp($now, $lastCheckout);
                            CheckinCheckout::submitCheckinAndOnBreak($userId, $now, $onBreakTimestamp, 0);
                            $onBreakTodaySum = CalcHelper::timeOnBreakSumForToday($userId);
                            $msg = Message::message($userId, $onBreakTodaySum, $breakTimeAllowed, 4);

                            //ako je nakon pauze došlo do prekoračenja pauze, pošalji mail --> gleda današnji dan
                            if ($onBreakTodaySum > $breakTimeAllowed) {
                                $dataMail = CheckinCheckout::where('user_keyId', $userId)->whereDate('checkout', Carbon::today())->get();
                                \Mail::to($mailTo)->send(new WarningMail($userMail, $dataMail, $onBreakTodaySum));
                            }
                            return new MessageResource($msg);

                            //insert into checkout (početak pauze)
                        } else if ($lastCheckin >= $lastCheckout) {
                            CheckinCheckout::submitCheckout($userId, $now);
                            $onBreakSum = CalcHelper::timeOnBreakSumForToday($userId);
                            $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 3);
                            return new MessageResource($msg);

                            //insert into checkout, prva pauza za tog usera (početak pauze)
                        } else if ($lastCheckin == 0 && $lastCheckout == 0) {
                            CheckinCheckout::submitCheckout($userId, $now);
                            $onBreakSum = CalcHelper::timeOnBreakSumForToday($userId);
                            $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 3);
                            return new MessageResource($msg);
                        }

                        //Error i obavijest da se moraju prijaviti sa jedinicom
                    } else if ($lastDeparture > $lastArrival || $lastDeparture == $lastArrival) {
                        $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 7);
                        return new MessageResource($msg);
                    } else {
                        $msg = Message::message($userId, 0, $breakTimeAllowed, 7);
                        return new MessageResource($msg);
                    }
                    break;

                //prijava sa jedinicom (arrival ili departure)
                case 1:
                    //insert into arrival
                    if ($lastArrival < $lastDeparture || $lastArrival == $lastDeparture) {
                        CheckinCheckout::submitArrival($userId, $now);
                        $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 1);
                        return new MessageResource($msg);

                        //insert into departure
                    } else if ($lastArrival > $lastDeparture) {

                        //insert into departure, ako je je prvi departure za korisnika i ako nikad nije bio na pauzi
                        if (($lastRowCheckin == 0) && ($lastRowCheckout == 0)) {
                            CheckinCheckout::submitDeparture($userId, $now);

                            //insert into departure, ako je zadnja pauza otvorena zatvori ju
                        } else if ($lastRowCheckin == 0) {
                            $onBreak = CalcHelper::dateStringDiffInTimestamp($now, $lastCheckout);
                            CheckinCheckout::submitCheckinAndOnBreak($userId, $now, $onBreak, 1);
                        }

                        //insert into departure, calculate Timestamp
                        $timeOnBreakToday = CalcHelper::timeOnBreakSumForToday($userId);
                        CheckinCheckout::submitDeparture($userId, $now);
                        $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 2);

                        //ako je došlo do prekoračenja pauze pošalji mail --> gleda današnji dan
                        if ($timeOnBreakToday > $breakTimeAllowed) {
                            $dataMail = CheckinCheckout::where('user_keyId', $userId)->whereDate('checkout', Carbon::today())->get();
                            \Mail::to($mailTo)->send(new WarningMail($userMail, $dataMail, $timeOnBreakToday));
                        }

                        return new MessageResource($msg);
                    } else {
                        $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 11);
                        return new MessageResource($msg);
                    }
                    break;

                default:
                    //error msg "Greška, drugo polje nije 0 ili 1. Morate se prijaviti sa 1 ili 2. Pokušajte opet!"
                    $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 8);
                    return new MessageResource($msg);
            }
    }
}
