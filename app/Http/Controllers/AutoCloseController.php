<?php

namespace App\Http\Controllers;

use App\CheckinCheckout;
use App\Classes\CalcHelper;
use App\Classes\GetData;
use App\Mail\AutoClose;
use App\User;
use Carbon\Carbon;

class AutoCloseController extends Controller
{
    public function autoClose(){

        $now = Carbon::now();
        $allUsers = User::all();
        $usersAutoClose = [];


        foreach($allUsers as $oneUser){

            $userId = $oneUser->keyId;

            $lastCheckout = GetData::getLastCheckout($userId)['checkout'];

            $lastArrival = GetData::getLastArrival($userId)['arrival'];
            $lastDeparture = GetData::getLastDeparture($userId)['departure'];

            $lastRowCheckin = CheckinCheckout::latest()->where('user_keyId', '=', $userId)->first()['checkin'];
            $lastRowCheckout = CheckinCheckout::latest()->where('user_keyId', '=', $userId)->first()['checkout'];

            $userMail = User::where('keyId', $userId)->first();

            $mailTo = 'tomislav@besoft.hr';


            if ($lastArrival > $lastDeparture) {

                //insert into departure, ako je je prvi departure za korisnika i ako nikad nije bio na pauzi
                if (($lastRowCheckin == 0) && ($lastRowCheckout == 0)) {
                    CheckinCheckout::submitDeparture($userId, $now);

                //insert into departure, ako je zadnja pauza otvorena zatvori ju
                } else if ($lastRowCheckin == 0) {
                    $onBreak = CalcHelper::dateStringDiffInTimestamp($now, $lastCheckout);
                    CheckinCheckout::submitCheckinAndOnBreak($userId, $now, $onBreak, 1);
                }

                //insert into departure, calculate Timestamp
                $timeOnBreakToday = CalcHelper::timeOnBreakSumForToday($userId);
                CheckinCheckout::submitDeparture($userId, $now);

                $usersAutoClose[] = $userId;



            }
        }

        if($usersAutoClose){
            \Mail::to($mailTo)->send(new AutoClose($usersAutoClose));
        }







    }
}
