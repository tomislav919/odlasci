<?php

namespace App\Http\Controllers;

use App\CheckinCheckout;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function redirectToReport()
    {

        $users = CheckinCheckout::with('User')->whereDate('created_at', Carbon::today())->get();

        return view('report')
            ->with('users', $users);
    }

}
