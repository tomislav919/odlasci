<?php

namespace App\Http\Controllers;

use App\CheckinCheckout;
use App\Classes\CalcHelper;
use App\Classes\GetData;
use App\Classes\Message;
use App\Http\Resources\Message as MessageResource;
use App\Mail\ForgotLogin;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;


class ForgottenFormController extends Controller
{
    public function formSubmit(Request $req)
    {
        $userId = $req['user_id'];
        $arrivalDeparture = $req['arrivalDeparture'];
        $arrivalTime = $req['arrivalTime'];

        $lastDeparture = GetData::getLastDeparture($userId)['departure'];
        $now = Carbon::now();
        $breakTimeAllowed = 1800;
        $onBreakSum = 0;

        $mailTo = 'adriano@inmobile-accessories.hr';
        $userMail = User::where('keyId', $userId)->first();
        $arrivalFullTime = CalcHelper::hoursMinutesToFullTime($arrivalTime);

        if($arrivalFullTime > $lastDeparture){

            CheckinCheckout::submitArrival($userId, $arrivalFullTime);
            \Mail::to($mailTo)->send(new ForgotLogin($userMail, $arrivalTime, $now));

            $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 1);
            return new MessageResource($msg);

        } else {

            $msg = Message::message($userId, $onBreakSum, $breakTimeAllowed, 10);
            return new MessageResource($msg);
        }

    }
}
