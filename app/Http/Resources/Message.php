<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Message extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
          'class' => $this->class,
          'id' => $this->id,
          'sound' => $this->sound,
          'denyMsg' => $this->denyMsg,
          'msg' => $this->msg,
          'forgottenArrival' => $this->forgottenArrival,
          'hideClock' => $this->hideClock
        ];

    }
}
