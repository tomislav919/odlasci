<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AutoClose extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($usersAutoClose)
    {
        //
            $this->user = User::whereIn('keyId', $usersAutoClose)->get();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($address = 'noreply@inmobile-accessories.hr', $name = 'Automatsko zatvaranje')
            ->subject('Automatsko zatvaranje')
            ->view('emails.autoClose');
    }
}
