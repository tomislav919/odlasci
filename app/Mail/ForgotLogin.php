<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotLogin extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $arrivalTime;
    public $now;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $arrivalTime, $now)
    {
        //
        $this->user = $user;

        $arrivalTime = $arrivalTime . ':00';
        $this->arrivalTime = $arrivalTime;

        $now = date("H:i:s", strtotime($now) );
        $this->now = $now;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($address = 'noreply@inmobile-accessories.hr', $name = 'Kašnjenje sa prijavom')
            ->subject('Kašnjenje sa prijavom')
            ->view('emails.forgotLogin');
    }
}
